import pytest
from omi_api.infra.file_system import (
    MetadataJsonV1,
    MetadataJsonV2,
    MetadataJsonV3,
)
from omi_api.infra.file_system.model.common import (
    detect_version_from_metadata_json,
)


@pytest.fixture()
def metadata_json_dict_v1():
    return {
        "indicator": {
            "created_at": "2021-08-17T09:53:45+00:00",
            "parameter_profile": "indre",
            "slug": "jupyter-notebook",
            "source_url": "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/tree/72ac027cc07d8166bf5cf1597e879dc6fde35d59",  # noqa
        }
    }


@pytest.fixture
def metadata_json_dict_v2():
    return {
        "indicator": {
            "created_at": "2021-08-17T09:53:45+00:00",
            "parameter_profile": {
                "name": "indre",
                "values": {"location_string": "Chabris, France"},
            },
            "slug": "jupyter-notebook",
            "source_url": "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/tree/72ac027cc07d8166bf5cf1597e879dc6fde35d59",  # noqa
        }
    }


@pytest.fixture
def metadata_json_dict_v3():
    return {
        "indicator": {
            "created_at": "2021-08-17T09:53:45+00:00",
            "parameter_profile": {
                "name": "indre",
                "values": {"location_string": "Chabris, France"},
            },
            "pipeline_id": "374453463",
            "slug": "jupyter-notebook",
            "source": {
                "branch": "my_branch",
                "commit": "7e67e0bb9d7ade2d052cf78042289f9f9be131bc",
                "project_url": "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook",
            },
        }
    }


def test_metadata_json_dict_v1(metadata_json_dict_v1):
    metadata_json = MetadataJsonV1.parse_obj(metadata_json_dict_v1)
    assert metadata_json is not None
    assert metadata_json.indicator.parameter_profile == "indre"

    indicator_data = metadata_json.to_domain(indicator_data_id="foo")
    assert indicator_data.id == "foo"
    assert indicator_data.parameter_profile.name == "indre"
    assert indicator_data.parameter_profile.values == {}
    assert indicator_data.pipeline_id == ""
    assert indicator_data.slug == "jupyter-notebook"
    assert indicator_data.source.branch == ""
    assert indicator_data.source.commit == "72ac027cc07d8166bf5cf1597e879dc6fde35d59"
    assert (
        indicator_data.source.project_url
        == "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook"
    )


def test_metadata_json_dict_v2(metadata_json_dict_v2):
    metadata_json = MetadataJsonV2.parse_obj(metadata_json_dict_v2)
    assert metadata_json is not None
    assert metadata_json.indicator.parameter_profile.name == "indre"
    assert len(metadata_json.indicator.parameter_profile.values) == 1
    assert metadata_json.indicator.parameter_profile.values == {
        "location_string": "Chabris, France"
    }

    indicator_data = metadata_json.to_domain(indicator_data_id="foo")
    assert indicator_data.id == "foo"
    assert indicator_data.parameter_profile.name == "indre"
    assert indicator_data.parameter_profile.values == {
        "location_string": "Chabris, France"
    }
    assert indicator_data.pipeline_id == ""
    assert indicator_data.slug == "jupyter-notebook"
    assert indicator_data.source.branch == ""
    assert indicator_data.source.commit == "72ac027cc07d8166bf5cf1597e879dc6fde35d59"
    assert (
        indicator_data.source.project_url
        == "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook"
    )


def test_metadata_json_dict_v3(metadata_json_dict_v3):
    metadata_json = MetadataJsonV3.parse_obj(metadata_json_dict_v3)
    assert metadata_json is not None
    assert metadata_json.indicator.parameter_profile.name == "indre"
    assert len(metadata_json.indicator.parameter_profile.values) == 1
    assert metadata_json.indicator.parameter_profile.values == {
        "location_string": "Chabris, France"
    }

    indicator_data = metadata_json.to_domain(indicator_data_id="foo")
    assert indicator_data.id == "foo"
    assert indicator_data.parameter_profile.name == "indre"
    assert indicator_data.parameter_profile.values == {
        "location_string": "Chabris, France"
    }
    assert indicator_data.pipeline_id == "374453463"
    assert indicator_data.slug == "jupyter-notebook"
    assert indicator_data.source.branch == "my_branch"
    assert indicator_data.source.commit == "7e67e0bb9d7ade2d052cf78042289f9f9be131bc"
    assert (
        indicator_data.source.project_url
        == "https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook"
    )


def test_detect_version_v1(metadata_json_dict_v1):
    version = detect_version_from_metadata_json(metadata_json_dict_v1)
    assert version == "1"


def test_detect_version_v2(metadata_json_dict_v2):
    version = detect_version_from_metadata_json(metadata_json_dict_v2)
    assert version == "2"


def test_detect_version_v3(metadata_json_dict_v3):
    version = detect_version_from_metadata_json(metadata_json_dict_v3)
    assert version == "3"
