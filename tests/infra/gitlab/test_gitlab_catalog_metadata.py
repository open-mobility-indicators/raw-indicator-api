import pytest
import yaml
from omi_api.infra.gitlab.model import GitLabCatalogMetadata


@pytest.fixture
def catalog_metadata_full():
    return yaml.safe_load(
        """
indicators:
    indicator_1:
        work_in_progress: true
        indicator_data:
            1234315:
                pre_selected: true
            1432522:
                pre_selected: true
"""
    )


@pytest.fixture
def catalog_metadata_empty_indicator_data():
    return yaml.safe_load(
        """
indicators:
    indicator_1:
        work_in_progress: true
        indicator_data: {}
"""
    )


@pytest.fixture
def catalog_metadata_without_indicator_data_1():
    return yaml.safe_load(
        """
indicators:
    indicator_1:
        work_in_progress: true
"""
    )


@pytest.fixture
def catalog_metadata_without_indicator_data_2():
    return yaml.safe_load(
        """
indicators:
    indicator_1:
        pre_selected: true
"""
    )


@pytest.fixture
def catalog_metadata_from_file():
    with open(
        "tests/infra/gitlab/fixtures/catalog-metadata.yml", "rt", encoding="utf-8"
    ) as fd:
        return yaml.safe_load(fd)


def test_parse_full(catalog_metadata_full):
    GitLabCatalogMetadata.parse_obj(catalog_metadata_full)


def test_parse_empty_indicator_data(catalog_metadata_empty_indicator_data):
    GitLabCatalogMetadata.parse_obj(catalog_metadata_empty_indicator_data)


def test_parse_without_indicator_data_1(catalog_metadata_without_indicator_data_1):
    gitlab_catalog_metadata = GitLabCatalogMetadata.parse_obj(
        catalog_metadata_without_indicator_data_1
    )
    catalog_metadata = gitlab_catalog_metadata.to_domain()
    assert "indicator_1" in catalog_metadata.indicators
    indicator_1 = catalog_metadata.indicators["indicator_1"]
    assert indicator_1.indicator_data is None


def test_parse_without_indicator_data_2(catalog_metadata_without_indicator_data_2):
    GitLabCatalogMetadata.parse_obj(catalog_metadata_without_indicator_data_2)


def test_parse_catalog_metadata_from_file(catalog_metadata_from_file):
    GitLabCatalogMetadata.parse_obj(catalog_metadata_from_file)
