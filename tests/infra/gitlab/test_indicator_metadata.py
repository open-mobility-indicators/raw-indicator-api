import pydantic
import pytest
import yaml
from omi_api.infra.gitlab.model import GitLabIndicatorMetadata


@pytest.fixture
def indicator_metadata_name_only():
    return yaml.safe_load(
        """
indicator:
    name: Un nom parlant
"""
    )


@pytest.fixture
def indicator_metadata_name_and_description():
    return yaml.safe_load(
        """
indicator:
    name: a name
    description: a description
"""
    )


@pytest.fixture
def indicator_metadata_full():
    return yaml.safe_load(
        """
indicator:
    name: a name
    description: a description
    popupTemplate: a popup template
"""
    )


@pytest.fixture
def indicator_metadata_no_name():
    return yaml.safe_load(
        """
indicator:
    description: a description
    popupTemplate: a popup template
"""
    )


def test_parse_indicator_metadata_1(indicator_metadata_name_only):
    indicator_metadata = GitLabIndicatorMetadata.parse_obj(indicator_metadata_name_only)
    assert indicator_metadata
    assert indicator_metadata.indicator.name == "Un nom parlant"
    assert indicator_metadata.indicator.description is None
    assert indicator_metadata.indicator.popupTemplate is None


def test_parse_indicator_metadata_2(indicator_metadata_name_and_description):
    indicator_metadata = GitLabIndicatorMetadata.parse_obj(
        indicator_metadata_name_and_description
    )
    assert indicator_metadata
    assert indicator_metadata.indicator.name == "a name"
    assert indicator_metadata.indicator.description == "a description"
    assert indicator_metadata.indicator.popupTemplate is None


def test_parse_indicator_metadata_3(indicator_metadata_full):
    indicator_metadata = GitLabIndicatorMetadata.parse_obj(indicator_metadata_full)
    assert indicator_metadata
    assert indicator_metadata.indicator.name == "a name"
    assert indicator_metadata.indicator.description == "a description"
    assert indicator_metadata.indicator.popupTemplate == "a popup template"


def test_parse_indicator_metadata_error(indicator_metadata_no_name):
    with pytest.raises(pydantic.ValidationError):
        GitLabIndicatorMetadata.parse_obj(indicator_metadata_no_name)
