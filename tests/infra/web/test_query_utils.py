import pytest
from omi_api.domain.repositories.indicator_data_repository import (
    IndicatorDataSortDef,
    IndicatorDataSortProperty,
)
from omi_api.infra.web.query_utils import (
    build_indicator_parameter_profiles_from_string,
    build_sortdef_list_from_string,
)


def build_sortdef_list(criteria: str):
    return build_sortdef_list_from_string(
        criteria, IndicatorDataSortProperty, IndicatorDataSortDef
    )


def test_empty_string():
    assert build_sortdef_list("") == []


def test_correct_string():
    criteria_str = "created_at:asc"
    sort_def_list = build_sortdef_list(criteria_str)
    assert len(sort_def_list) == 1
    assert sort_def_list[0].property_name == "created_at"
    assert sort_def_list[0].order == "asc"


def test_multi_correct_string():
    criteria_str = "created_at:asc,slug:desc"
    sort_def_list = build_sortdef_list(criteria_str)
    assert len(sort_def_list) == 2
    assert sort_def_list[0].property_name == "created_at"
    assert sort_def_list[0].order == "asc"
    assert sort_def_list[1].property_name == "slug"
    assert sort_def_list[1].order == "desc"


def test_non_existing_property_string():
    criteria_str = "foo:asc"

    with pytest.raises(SyntaxError):
        build_sortdef_list(criteria_str)


def test_invalid_order():
    criteria_str = "created_at:xxx"

    with pytest.raises(SyntaxError):
        build_sortdef_list(criteria_str)


def test_parameter_profiles_all():
    input_str = "*"
    assert build_indicator_parameter_profiles_from_string(input_str) == "all"


def test_parameter_profiles_all_with_spaces():
    input_str = "*  "
    assert build_indicator_parameter_profiles_from_string(input_str) == "all"


def test_parameter_profiles_list():
    input_str = "abc,bcd"
    assert build_indicator_parameter_profiles_from_string(input_str) == ["abc", "bcd"]


def test_parameter_profiles_list_trailing_comma():
    input_str = "abc,bcd,"
    assert build_indicator_parameter_profiles_from_string(input_str) == ["abc", "bcd"]


def test_parameter_profiles_list_leading_commas():
    input_str = ",,,abc,bcd,"
    assert build_indicator_parameter_profiles_from_string(input_str) == ["abc", "bcd"]


def test_parameter_profiles_list_with_spaces():
    input_str = " abc  , bcd  "
    assert build_indicator_parameter_profiles_from_string(input_str) == ["abc", "bcd"]


def test_parameter_profiles_single_element_list():
    input_str = "abcdef"
    assert build_indicator_parameter_profiles_from_string(input_str) == ["abcdef"]


def test_parameter_profiles_empy_list():
    with pytest.raises(SyntaxError):
        build_indicator_parameter_profiles_from_string("  ")
