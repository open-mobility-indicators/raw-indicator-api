from omi_api.infra.sqlite.utils import compute_sortable_str


def test_case():
    assert compute_sortable_str("AaBbYy") == "aabbyy"


def test_accent():
    assert compute_sortable_str("éléphant à côté") == "elephant a cote"


def test_case_accent():
    assert compute_sortable_str("Échec et Mathématiques") == "echec et mathematiques"


def test_special_letters():
    assert (
        compute_sortable_str("Œufs et Encyclopædia Universalis")
        == "oeufs et encyclopaedia universalis"
    )
