import pytest
from omi_api.domain.repositories.common import PaginationDef
from omi_api.domain.repositories.indicator_data_repository import (
    IndicatorDataFilterDef,
    IndicatorDataSortDef,
)
from omi_api.infra.sqlite import SQLiteIndicatorDataRepo

from sample_data_fixture import sample_indicator_data_list


@pytest.fixture()
def sqlite_repo() -> SQLiteIndicatorDataRepo:
    return SQLiteIndicatorDataRepo.from_indicator_data(
        indicator_data=sample_indicator_data_list
    )


def test_get_all(sqlite_repo: SQLiteIndicatorDataRepo):
    assert sqlite_repo.get("AAA") is not None
    assert sqlite_repo.get("AAA") == sample_indicator_data_list[0]
    assert sqlite_repo.get("BBB") is not None
    assert sqlite_repo.get("BBB") == sample_indicator_data_list[1]
    assert sqlite_repo.get("CCC") is not None
    assert sqlite_repo.get("CCC") == sample_indicator_data_list[2]
    assert sqlite_repo.get("DDD") is not None
    assert sqlite_repo.get("DDD") == sample_indicator_data_list[3]


def build_indicator_id_list(sqlite_repo: SQLiteIndicatorDataRepo, **kwargs):
    return [indicator.id for indicator in sqlite_repo.find(**kwargs)]


def test_find_without_filter(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo, filter_by=IndicatorDataFilterDef()
    )
    assert indicator_id_list == ["AAA", "BBB", "CCC", "DDD"]


def test_find_filter_by_slug(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo, filter_by=IndicatorDataFilterDef(slug="cycles")
    )
    assert indicator_id_list == ["AAA", "CCC"]


def test_find_filter_by_parameter_profile(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo, filter_by=IndicatorDataFilterDef(parameter_profile="indre")
    )
    assert indicator_id_list == ["AAA", "BBB"]


def test_find_filter_by_slug_and_parameter_profile(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(slug="cycles", parameter_profile="indre"),
    )

    assert indicator_id_list == ["AAA"]


def test_sort_by_created_at_asc(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        sort_by=[IndicatorDataSortDef(property_name="created_at", order="asc")],
    )

    assert indicator_id_list == ["AAA", "CCC", "DDD", "BBB"]


def test_sort_by_created_at_desc(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        sort_by=[IndicatorDataSortDef(property_name="created_at", order="desc")],
    )

    assert indicator_id_list == ["BBB", "DDD", "CCC", "AAA"]


def test_multi_sort(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        sort_by=[
            IndicatorDataSortDef(property_name="slug", order="asc"),
            IndicatorDataSortDef(property_name="created_at", order="desc"),
        ],
    )

    assert indicator_id_list == ["CCC", "AAA", "BBB", "DDD"]


def test_filter_and_sort(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(slug="cycles"),
        sort_by=[
            IndicatorDataSortDef(property_name="created_at", order="desc"),
        ],
    )

    assert indicator_id_list == ["CCC", "AAA"]


def test_find_no_filter_one_page(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        paginate_by=PaginationDef(page=1, size=4),
    )
    assert indicator_id_list == ["AAA", "BBB", "CCC", "DDD"]


def test_find_no_filter_two_pages_one(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        paginate_by=PaginationDef(page=1, size=2),
    )
    assert indicator_id_list == ["AAA", "BBB"]


def test_find_no_filter_two_pages_two(sqlite_repo: SQLiteIndicatorDataRepo):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(),
        paginate_by=PaginationDef(page=2, size=2),
    )
    assert indicator_id_list == ["CCC", "DDD"]


def test_filter_sort_paginate(
    sqlite_repo: SQLiteIndicatorDataRepo,
):
    indicator_id_list = build_indicator_id_list(
        sqlite_repo,
        filter_by=IndicatorDataFilterDef(slug="cycles"),
        sort_by=[
            IndicatorDataSortDef(property_name="created_at", order="desc"),
        ],
        paginate_by=PaginationDef(page=2, size=1),
    )

    assert indicator_id_list == ["AAA"]
