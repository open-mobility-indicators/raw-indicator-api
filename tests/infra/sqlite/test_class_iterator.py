from dataclasses import dataclass


@dataclass
class MyRepository:
    data: list[int]

    def add(self, nb: int):
        self.data.append(nb)

    def remove(self, nb: int):
        self.data.remove(nb)

    def value_iter(self):
        def values():
            yield from self.data

        return values


def test_iter():
    my_repo = MyRepository(data=[1, 2, 3, 4])

    val_iter = my_repo.value_iter()
    assert list(val_iter()) == [1, 2, 3, 4]
    assert list(val_iter()) == [1, 2, 3, 4]

    my_repo.add(5)
    assert list(val_iter()) == [1, 2, 3, 4, 5]
