from datetime import datetime

from omi_api.domain import IndicatorData, ParameterProfile, IndicatorSource


def test_init():
    indicator_data = IndicatorData(
        id="my_id",
        created_at=datetime.now(),
        parameter_profile=ParameterProfile(
            name="my_parameter_profile", values={"a": "A"}
        ),
        pipeline_id="123456",
        slug="my-slug",
        source=IndicatorSource(
            branch="main",
            commit="AAAAAA",
            project_url="https://gitlab.com/open-mobility-indicators/indicators/cycles",
        ),
    )
    assert indicator_data.id == "my_id"
    assert indicator_data.parameter_profile.name == "my_parameter_profile"
    assert indicator_data.parameter_profile.values == {"a": "A"}
    assert indicator_data.pipeline_id == "123456"
    assert indicator_data.slug == "my-slug"
    assert indicator_data.source.branch == "main"
    assert indicator_data.source.commit == "AAAAAA"
    assert (
        indicator_data.source.project_url
        == "https://gitlab.com/open-mobility-indicators/indicators/cycles"
    )
