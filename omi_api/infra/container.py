from dependency_injector import containers, providers
from omi_api.domain import IndicatorDataService, IndicatorService
from omi_api.domain.repositories.indicator_data_repository import (
    FileSystemIndicatorDataRepository,
    FileSystemMbtilesRepository,
    IndicatorDataRepository,
)
from omi_api.domain.repositories.indicator_repository import (
    IndicatorPipelineRepository,
    IndicatorRepository,
)
from omi_api.domain.repositories.metadata_repository import (
    CatalogMetadataRepository,
    IndicatorMetadataRepository,
)
from omi_api.infra.file_system import FileSystemIndicatorDataRepo, FileSystemMbtilesRepo
from omi_api.infra.gitlab.catalog_metadata_repo import CatalogMetadataRepo
from omi_api.infra.gitlab.indicator_metadata_repo import IndicatorMetadataRepo
from omi_api.infra.gitlab.indicator_pipeline_repo import IndicatorPipelineRepo
from omi_api.infra.sqlite import SQLiteIndicatorDataRepo, SQLiteIndicatorRepo
from omi_api.infra.web.model import IndicatorMapper

__all__ = ["Container"]


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()

    file_system_indicator_data_repo: FileSystemIndicatorDataRepository = providers.Factory(  # type: ignore
        FileSystemIndicatorDataRepo,
        indicator_data_base_dir=config.indicator_data_base_dir,
        dry_run=config.dry_run,
    )

    file_system_mbtiles_repo: FileSystemMbtilesRepository = providers.Factory(  # type: ignore
        FileSystemMbtilesRepo,
        tileset_dir=config.tileset_dir,
        mbtileserver_container_name=config.mbtileserver_container_name,
        dry_run=config.dry_run,
    )

    indicator_data_repo: IndicatorDataRepository = providers.Singleton(  # type: ignore
        SQLiteIndicatorDataRepo
    )

    indicator_metadata_repo: IndicatorMetadataRepository = providers.Factory(  # type: ignore
        IndicatorMetadataRepo,
        gitlab_indicator_group_url=config.gitlab_indicator_group_url,
    )

    indicator_pipeline_repo: IndicatorPipelineRepository = providers.Singleton(  # type: ignore
        IndicatorPipelineRepo,
        gitlab_indicator_group_url=config.gitlab_indicator_group_url,
        gitlab_private_token=config.gitlab_private_token,
    )

    indicator_repo: IndicatorRepository = providers.Singleton(  # type: ignore
        SQLiteIndicatorRepo,
    )

    catalog_metadata_repo: CatalogMetadataRepository = providers.Singleton(  # type: ignore
        CatalogMetadataRepo,
        catalog_metadata_url=config.catalog_metadata_url,
    )

    indicator_data_service: IndicatorDataService = providers.Factory(  # type: ignore
        IndicatorDataService,
        file_system_indicator_data_repo=file_system_indicator_data_repo,
        file_system_mbtiles_repo=file_system_mbtiles_repo,
        indicator_data_repo=indicator_data_repo,
        catalog_metadata_repo=catalog_metadata_repo,
    )

    indicator_service: IndicatorService = providers.Factory(  # type: ignore
        IndicatorService,
        indicator_data_service=indicator_data_service,
        catalog_metadata_repo=catalog_metadata_repo,
        indicator_metadata_repo=indicator_metadata_repo,
        indicator_repo=indicator_repo,
        indicator_pipeline_repo=indicator_pipeline_repo,
    )

    indicator_mapper: IndicatorMapper = providers.Factory(
        IndicatorMapper,
        indicator_data_service=indicator_data_service,
        indicator_service=indicator_service,
    )
