from dependency_injector.wiring import Provide, inject
from fastapi import Depends, HTTPException, Request
from omi_api.domain.repositories.common import ReloadSubject
from omi_api.domain.services.indicator_service import IndicatorService
from omi_api.infra.container import Container
from omi_api.infra.web.model import CommandReportDTO, IndicatorMapper
from omi_api.infra.web.query_utils import extract_admin_token


@inject
async def reload(
    request: Request,
    admin_token: str = None,  # Make it appear in swagger interface for practical use
    subject: ReloadSubject = None,
    reference_admin_token: str = Depends(Provide[Container.config.admin_token]),
    indicator_service: IndicatorService = Depends(Provide[Container.indicator_service]),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
) -> CommandReportDTO:
    """Reload indicator data and metadata."""
    user_token = await extract_admin_token(request)
    if user_token is None or user_token != reference_admin_token:
        raise HTTPException(status_code=403, detail="Wrong or missing token")

    report = indicator_service.reload(subject=subject)
    return indicator_mapper.command_report_to_dto(report)
