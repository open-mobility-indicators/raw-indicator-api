from typing import Mapping
from urllib.parse import urljoin

from fastapi import APIRouter, Request
from omi_api.infra.web.model import (
    CommandReportDTO,
    IndicatorDataListWithStatsDTO,
    IndicatorDataWithStatsDTO,
    IndicatorListWithStatsDTO,
    IndicatorWithStatsDTO,
)
from omi_api.infra.web.router.indicator import (
    delete_indicator_by_slug,
    get_indicator_by_slug,
    list,
    run_indicator_pipelines,
)
from omi_api.infra.web.router.indicator_data import (
    delete_indicator_data_by_id,
    find,
    get_indicator_data_by_id,
)

from .reload import reload

__all__ = ["router"]


router = APIRouter()


@router.get("/", include_in_schema=False)
async def root(request: Request):
    apidocs_url = urljoin(str(request.base_url), "docs")
    return {
        "message": (
            f"Open Mobility Indicators API doc can be found there: {apidocs_url}"
        )
    }


# Hide model attributes with null or default value
response_model_excludes: Mapping = {
    "response_model_exclude_none": True,
    "response_model_exclude_defaults": True,
}

# Admin endpoints.

router.put(
    "/_reload",
    response_model=CommandReportDTO,
    **response_model_excludes,
    tags=["admin"],
)(reload)


# Endpoints about Indicator entity.

router.get(
    "/indicator",
    response_model=IndicatorListWithStatsDTO,
    **response_model_excludes,
    tags=["indicator"],
)(list)

router.get(
    "/indicator/{indicator_slug}",
    response_model=IndicatorWithStatsDTO,
    **response_model_excludes,
    tags=["indicator"],
)(get_indicator_by_slug)

router.delete(
    "/indicator/{indicator_slug}",
    response_model=CommandReportDTO,
    **response_model_excludes,
    tags=["indicator"],
)(delete_indicator_by_slug)

router.post(
    "/indicator/{indicator_slug}/run_pipelines",
    response_model=CommandReportDTO,
    **response_model_excludes,
    tags=["indicator"],
)(run_indicator_pipelines)


# Endpoints about IndicatorData entity.

router.get(
    "/indicator-data",
    response_model=IndicatorDataListWithStatsDTO,
    **response_model_excludes,
    tags=["indicator data"],
)(find)

router.get(
    "/indicator-data/{indicator_data_id}",
    response_model=IndicatorDataWithStatsDTO,
    **response_model_excludes,
    tags=["indicator data"],
)(get_indicator_data_by_id)

router.delete(
    "/indicator-data/{indicator_data_id}",
    response_model=CommandReportDTO,
    **response_model_excludes,
    tags=["indicator data"],
)(delete_indicator_data_by_id)
