import importlib.metadata

from dependency_injector.wiring import Provide, inject
from fastapi import FastAPI
from omi_api.domain.services.indicator_data_service import IndicatorDataService
from omi_api.domain.services.indicator_service import IndicatorService
from omi_api.infra.container import Container
from omi_api.infra.web.router import router

__all__ = ["create_app"]

# Get version from pyproject.toml file
VERSION = importlib.metadata.version("omi_api")


@inject
def create_app(
    indicator_data_service: IndicatorDataService = Provide[
        Container.indicator_data_service
    ],
    indicator_service: IndicatorService = Provide[Container.indicator_service],
) -> FastAPI:
    tags_metadata = [
        {"name": "indicator", "description": "Indicator endpoints"},
        {"name": "indicator data", "description": "Indicator data endpoints"},
        {"name": "admin", "description": "Administration endpoints"},
    ]
    app = FastAPI(
        title="Open Mobility Indicators API",
        version=VERSION,
        openapi_tags=tags_metadata,
    )
    app.include_router(router)

    indicator_service.reload()

    return app
