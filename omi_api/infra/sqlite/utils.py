import unicodedata
from typing import Optional


def dict_factory(cursor, row):
    """Return row as dict instead of tuple."""
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def build_order_string_from_sort_def(
    sort_by, *, column_aliases: Optional[dict[str, str]] = None
) -> str:
    """Build ORDER BY string from sort criteria.

    Column names can be changed using column_aliases dict
    """
    if not sort_by:
        return ""

    col_dict = {} if column_aliases is None else column_aliases

    def format_sort_criteria(sd) -> str:
        col_name = col_dict.get(sd.property_name.value, sd.property_name.value)
        return f"{col_name} {sd.order.value.upper()}"

    return " ORDER BY " + ", ".join(format_sort_criteria(sd) for sd in sort_by)


def compute_sortable_str(text: str) -> str:
    """Remove accents and lowercase the given text."""
    text = text.lower()
    text = "".join(
        c for c in unicodedata.normalize("NFD", text) if unicodedata.category(c) != "Mn"
    )
    text = text.replace("œ", "oe")
    text = text.replace("æ", "ae")
    return text
