import logging
import sqlite3
from typing import Iterator, Optional

from omi_api.domain.entities.indicator import Indicator, IndicatorSlug
from omi_api.domain.repositories.common import PaginationDef
from omi_api.domain.repositories.indicator_repository import IndicatorSortDef
from omi_api.domain.repositories.metadata_repository import (
    CatalogMetadataRepository,
    IndicatorMetadataRepository,
)

from .model import SqliteIndicatorRow
from .utils import build_order_string_from_sort_def, compute_sortable_str, dict_factory

__all__ = ["SQLiteIndicatorRepo"]

TABLE_NAME = "indicator"
CREATE_TABLE_QUERY = f"""
    CREATE TABLE IF NOT EXISTS {TABLE_NAME} (
        slug TEXT PRIMARY KEY,
        name TEXT,
        _name TEXT,
        description TEXT,
        popup_template TEXT,
        pre_selected BOOLEAN NOT NULL CHECK (pre_selected IN (0, 1)),
        work_in_progress BOOLEAN NOT NULL CHECK (work_in_progress IN (0,1))
    )
"""
UPSERT_QUERY = f"""
    INSERT OR REPLACE INTO {TABLE_NAME}
        (slug, name, _name, description, popup_template, pre_selected, work_in_progress)
        VALUES(?, ?, ?, ?, ?, ?, ?)
"""

log = logging.getLogger(__name__)


class SQLiteIndicatorRepo:
    """Repository loading Indicator entities from a SQLite database."""

    def __init__(self):
        self._conn = sqlite3.connect(":memory:")
        self._conn.row_factory = dict_factory

        cur = self._conn.cursor()
        cur.execute(CREATE_TABLE_QUERY)
        self._conn.commit()

    def reload_from_slug_iter(
        self,
        slug_iter: Iterator[IndicatorSlug],
        *,
        indicator_metadata_repo: IndicatorMetadataRepository,
        catalog_metadata_repo: CatalogMetadataRepository,
    ):
        cur = self._conn.cursor()
        cur.execute(f"DELETE FROM {TABLE_NAME}")

        for indicator_slug in slug_iter:
            log.debug("Indicator slug: %s", indicator_slug)
            indicator_metadata = indicator_metadata_repo.get_indicator_metadata(
                indicator_slug
            )
            if indicator_metadata is None:
                continue
            indicator_info = catalog_metadata_repo.get_indicator_info(indicator_slug)
            pre_selected = (
                indicator_info is not None and indicator_info.pre_selected is True
            )
            work_in_progress = (
                indicator_info is not None and indicator_info.work_in_progress is True
            )
            cur.execute(
                UPSERT_QUERY,
                (
                    indicator_slug,
                    indicator_metadata.indicator.name,
                    # _name column contain a "sortable" name, i.e: lowercase and without accents
                    # This allow to sort case insensitive and handle accented letters (îèàç) as ascii ones (ieac)
                    compute_sortable_str(indicator_metadata.indicator.name),
                    indicator_metadata.indicator.description,
                    indicator_metadata.indicator.popupTemplate,
                    pre_selected,
                    work_in_progress,
                ),
            )
        self._conn.commit()

    def get(self, indicator_slug: IndicatorSlug) -> Optional[Indicator]:
        cur = self._conn.cursor()
        cur.execute(f"SELECT * FROM {TABLE_NAME} WHERE slug=?", (indicator_slug,))
        row = cur.fetchone()
        if row is None:
            return None
        return SqliteIndicatorRow.parse_obj(row).to_domain()

    def delete_by_slug(self, indicator_slug: IndicatorSlug) -> bool:
        cur = self._conn.cursor()
        cur.execute(f"DELETE FROM {TABLE_NAME} WHERE slug=?", (indicator_slug,))
        return cur.rowcount != 0

    def list(
        self,
        *,
        pre_selected: bool,
        sort_by: Optional[list[IndicatorSortDef]] = None,
        paginate_by: Optional[PaginationDef] = None,
    ) -> Iterator[Indicator]:

        where_string = compute_where_string(pre_selected)

        # Use '_name' column to sort on name
        order_string = build_order_string_from_sort_def(
            sort_by, column_aliases={"name": "_name"}
        )

        pagination_string, pagination_values = "", []
        if paginate_by:
            pagination_string = " LIMIT ? OFFSET ?"
            pagination_values = [paginate_by.limit, paginate_by.offset]

        query_values = pagination_values
        cur = self._conn.cursor()
        cur.execute(
            f"SELECT * FROM {TABLE_NAME}{where_string}{order_string}{pagination_string}",
            query_values,
        )

        for row in cur.fetchall():
            yield SqliteIndicatorRow.parse_obj(row).to_domain()

    def count(
        self,
        pre_selected: bool,
    ) -> int:

        where_string = compute_where_string(pre_selected)

        cur = self._conn.cursor()
        cur.execute(
            f"SELECT COUNT(*) AS total FROM {TABLE_NAME}{where_string}",
        )

        return cur.fetchone()["total"]


def compute_where_string(pre_selected: bool) -> str:
    return " WHERE pre_selected=1" if pre_selected else ""
