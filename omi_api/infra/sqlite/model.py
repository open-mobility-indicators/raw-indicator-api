from datetime import datetime
from typing import Any, Optional, cast

from omi_api.domain import Indicator, IndicatorData, IndicatorSource, ParameterProfile
from pydantic import AnyHttpUrl, BaseModel
from pydantic.types import Json


class SqliteIndicatorRow(BaseModel):
    """Parser class for an indicator row."""

    description: Optional[str] = None
    name: str
    popup_template: Optional[str] = None
    slug: str
    pre_selected: bool
    work_in_progress: bool

    def to_domain(self) -> Indicator:
        """Transform instance to a Indicator instance of the domain model."""
        return Indicator(
            description=self.description if self.description else None,
            name=self.name,
            popup_template=self.popup_template if self.popup_template else None,
            pre_selected=self.pre_selected,
            slug=self.slug,
            work_in_progress=self.work_in_progress,
        )


class SqliteIndicatorDataRow(BaseModel):
    """Parser class for an indicator data row."""

    created_at: datetime
    id: str
    parameter_profile: str
    parameter_profile_values: Json
    pipeline_id: str
    slug: str
    source_branch: str
    source_commit: str
    source_project_url: AnyHttpUrl

    def to_domain(self) -> IndicatorData:
        """Transform instance to a IndicatorData instance of the domain model."""
        values = cast(dict[str, Any], self.parameter_profile_values)
        return IndicatorData(
            id=self.id,
            created_at=self.created_at,
            parameter_profile=ParameterProfile(
                name=self.parameter_profile, values=values
            ),
            pipeline_id=self.pipeline_id,
            slug=self.slug,
            source=IndicatorSource(
                branch=self.source_branch,
                commit=self.source_commit,
                project_url=self.source_project_url,
            ),
        )
