from typing import Any
from urllib.parse import urlparse

from omi_api.domain.entities.indicator import IndicatorSlug
from omi_api.domain.repositories.indicator_repository import (
    GitLabIndicatorProject,
    IndicatorPipelineRepository,
    PipelineID,
)
from omi_api.infra.gitlab.errors import BranchNotFound, GitLabProjectNotFound

import gitlab


class IndicatorPipelineRepo(IndicatorPipelineRepository):
    _gitlab_private_token: str
    _gitlab_url: str
    _gitlab_indicator_project_path: str
    _gl: Any

    def __init__(self, *, gitlab_indicator_group_url: str, gitlab_private_token: str):
        self._gitlab_private_token = gitlab_private_token
        o = urlparse(gitlab_indicator_group_url)
        self._gitlab_url = f"{o.scheme}://{o.netloc}"
        self.group_project_path = o.path.lstrip("/")

        self._gl = self._auth()

    def _auth(self):
        gl = gitlab.Gitlab(
            self._gitlab_url, private_token=self._gitlab_private_token, api_version="4"
        )
        gl.auth()
        return gl

    def _retrieve_parameter_profiles_from_project(self, project, ref: str) -> set[str]:

        return {
            item["name"].removesuffix(".json")
            for item in project.repository_tree(
                path="parameter_profiles", ref=ref, as_list=False
            )
            if item["name"].endswith(".json")
        }

    def _valid_ref(self, project, branch_name: str = None) -> str:
        """Check that given branch name is valid for the project.

        If no branch name is given, default branch is returned.
        """
        if branch_name is None:
            return project.default_branch

        if branch_name not in [b.name for b in project.branches.list()]:
            raise BranchNotFound(f"branch not found: {branch_name!r}")
        return branch_name

    def get_project_by_slug_and_branch(
        self, indicator_slug: IndicatorSlug, branch_name: str = None
    ) -> GitLabIndicatorProject:
        project_name_with_namespace = f"{self.group_project_path}/{indicator_slug}"
        try:
            project = self._gl.projects.get(project_name_with_namespace)
        except gitlab.exceptions.GitlabGetError:
            raise GitLabProjectNotFound(
                f"Can't retrieve project by its slug: {indicator_slug!r}"
            )

        ref = self._valid_ref(project, branch_name)

        return GitLabIndicatorProject(
            project_id=project.id,
            ref=ref,
            parameter_profiles=self._retrieve_parameter_profiles_from_project(
                project, ref=ref
            ),
        )

    def run_pipeline(
        self,
        gitlab_indicator_project: GitLabIndicatorProject,
        parameter_profile: str,
    ) -> PipelineID:

        project = self._gl.projects.get(gitlab_indicator_project.project_id, lazy=True)
        pipeline = project.pipelines.create(
            {
                "ref": gitlab_indicator_project.ref,
                "variables": [
                    {"key": "PARAMETER_PROFILE_NAME", "value": parameter_profile}
                ],
            }
        )
        return pipeline.id
