import logging
from urllib.parse import urljoin

import pydantic
import requests
import yaml
from omi_api.domain.entities.indicator import IndicatorSlug
from omi_api.domain.repositories.metadata_repository import IndicatorMetadataRepository
from pydantic import AnyHttpUrl

from .errors import (
    IndicatorMetadataError,
    IndicatorMetadataNotFound,
    InvalidIndicatorMetadata,
    InvalidYaml,
)
from .model import GitLabIndicatorMetadata

log = logging.getLogger(__name__)


__all__ = ["IndicatorMetadataRepo"]


class IndicatorMetadataRepo(IndicatorMetadataRepository):

    _gitlab_indicator_group_url: AnyHttpUrl
    _indicator_metadata_dict: dict[IndicatorSlug, GitLabIndicatorMetadata]

    def __init__(self, gitlab_indicator_group_url: AnyHttpUrl):
        self._gitlab_indicator_group_url = gitlab_indicator_group_url
        self._indicator_metadata_dict = {}

    def get_indicator_metadata(self, indicator_slug: IndicatorSlug):
        if indicator_slug in self._indicator_metadata_dict:
            return self._indicator_metadata_dict[indicator_slug]

        indicator_metadata = None
        try:
            indicator_metadata = self._fetch(indicator_slug)
        except IndicatorMetadataError as ime:
            log.exception(
                f"Can't retrieve indicator metadata {ime.indicator_slug!r} from {ime.indicator_metadata_url!r}"
            )
            return None
        self._indicator_metadata_dict[indicator_slug] = indicator_metadata
        return indicator_metadata

    def _fetch(self, indicator_slug: IndicatorSlug) -> GitLabIndicatorMetadata:
        project_url = urljoin(self._gitlab_indicator_group_url, "indicators")
        indicator_metadata_url = (
            f"{project_url}/{indicator_slug}/-/raw/main/indicator.yaml"
        )

        req = requests.get(indicator_metadata_url)
        if not req.ok:
            raise IndicatorMetadataNotFound(
                indicator_slug=indicator_slug,
                indicator_metadata_url=indicator_metadata_url,
            )
        try:
            yaml_obj = yaml.safe_load(req.content)
        except yaml.scanner.ScannerError:
            raise InvalidYaml(
                indicator_slug=indicator_slug,
                indicator_metadata_url=indicator_metadata_url,
            )
        try:
            return GitLabIndicatorMetadata.parse_obj(yaml_obj)
        except pydantic.ValidationError:
            raise InvalidIndicatorMetadata(
                indicator_slug=indicator_slug,
                indicator_metadata_url=indicator_metadata_url,
            )

    def refresh(self):
        self._indicator_metadata_dict.clear()
