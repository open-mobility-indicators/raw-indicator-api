from typing import Optional

from omi_api.domain.entities.catalog import (
    CatalogIndicatorDataMetadata,
    CatalogIndicatorMetadata,
    CatalogMetadata,
)
from omi_api.domain.entities.indicator import Indicator, IndicatorSlug
from omi_api.domain.entities.indicator_data import IndicatorDataId
from pydantic import BaseModel

__all__ = ["GitLabIndicatorMetadata"]


class GitLabIndicatorMetadataProperties(BaseModel):
    name: str
    description: Optional[str] = None
    popupTemplate: Optional[str] = None


class GitLabIndicatorMetadata(BaseModel):
    indicator: GitLabIndicatorMetadataProperties

    def to_domain(self, *, slug: IndicatorSlug) -> Indicator:
        return Indicator(
            slug=slug,
            name=self.indicator.name,
            description=self.indicator.description,
            popup_template=self.indicator.popupTemplate,
            pre_selected=False,
            work_in_progress=False,
        )


class GitLabCatalogIndicatorDataMetadata(BaseModel):
    pre_selected: bool

    def to_domain(self) -> CatalogIndicatorDataMetadata:
        return CatalogIndicatorDataMetadata(pre_selected=self.pre_selected)


class GitLabCatalogIndicatorMetadata(BaseModel):
    pre_selected: Optional[bool] = False
    work_in_progress: Optional[bool] = False
    indicator_data: Optional[
        dict[IndicatorDataId, GitLabCatalogIndicatorDataMetadata]
    ] = None

    def to_domain(self) -> CatalogIndicatorMetadata:
        indicator_data = (
            None
            if self.indicator_data is None
            else {k: v.to_domain() for k, v in self.indicator_data.items()}
        )

        return CatalogIndicatorMetadata(
            pre_selected=self.pre_selected,
            work_in_progress=self.work_in_progress,
            indicator_data=indicator_data,
        )


class GitLabCatalogMetadata(BaseModel):
    indicators: dict[IndicatorSlug, GitLabCatalogIndicatorMetadata]

    def to_domain(self) -> CatalogMetadata:
        indicators = {k: v.to_domain() for k, v in self.indicators.items()}
        return CatalogMetadata(indicators=indicators)
