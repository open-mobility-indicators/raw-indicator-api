import logging
from typing import Optional

import pydantic
import requests
import yaml
from omi_api.domain.entities.catalog import CatalogIndicatorMetadata, CatalogMetadata
from omi_api.domain.repositories.metadata_repository import CatalogMetadataRepository
from pydantic import AnyHttpUrl

from .errors import (
    CatalogMetadataNotFound,
    InvalidCatalogMetadata,
    InvalidCatalogMetadataFormat,
)
from .model import GitLabCatalogMetadata

log = logging.getLogger(__name__)


__all__ = ["CatalogMetadataRepo"]


class CatalogMetadataRepo(CatalogMetadataRepository):

    _catalog_metadata_url: AnyHttpUrl
    _catalog_metadata: Optional[CatalogMetadata] = None

    def __init__(self, catalog_metadata_url: AnyHttpUrl):
        self._catalog_metadata_url = catalog_metadata_url
        self.reload()

    def get(self) -> Optional[CatalogMetadata]:
        return self._catalog_metadata

    def _fetch(self) -> Optional[GitLabCatalogMetadata]:
        req = requests.get(self._catalog_metadata_url)
        if not req.ok:
            raise CatalogMetadataNotFound(
                catalog_metadata_url=self._catalog_metadata_url
            )
        try:
            yaml_obj = yaml.safe_load(req.content)
        except yaml.scanner.ScannerError:
            raise InvalidCatalogMetadataFormat(
                catalog_metadata_url=self._catalog_metadata_url
            )
        try:
            return GitLabCatalogMetadata.parse_obj(yaml_obj)
        except pydantic.ValidationError:
            raise InvalidCatalogMetadata(
                catalog_metadata_url=self._catalog_metadata_url
            )

    def get_indicator_info(
        self, indicator_slug: str
    ) -> Optional[CatalogIndicatorMetadata]:
        return (
            self._catalog_metadata.get_indicator_info(indicator_slug)
            if self._catalog_metadata
            else None
        )

    def is_preselected(self, indicator_slug: str, indicator_data_id: str) -> bool:
        return (
            self._catalog_metadata.is_preselected(indicator_slug, indicator_data_id)
            if self._catalog_metadata
            else False
        )

    def reload(self):
        gitlab_catalog_metadata = self._fetch()
        if gitlab_catalog_metadata:
            self._catalog_metadata = gitlab_catalog_metadata.to_domain()
