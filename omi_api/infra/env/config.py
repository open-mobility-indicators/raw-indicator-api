"""Read config variables from env"""

from pydantic import BaseSettings, DirectoryPath
from pydantic.networks import AnyHttpUrl

__all__ = ["Settings"]


class Settings(BaseSettings):
    indicator_data_base_dir: DirectoryPath
    tileset_dir: DirectoryPath
    admin_token: str
    dry_run: bool = False
    gitlab_indicator_group_url: AnyHttpUrl
    gitlab_private_token: str
    catalog_metadata_url: AnyHttpUrl
    mbtileserver_container_name: str

    class Config:
        env_prefix = "OMI_API_"
