from dataclasses import dataclass
from pathlib import Path

__all__ = [
    "DetectVersionError",
    "FileSystemAdapterError",
    "IndicatorDataDirError",
    "IndicatorDataDirNotFound",
    "InvalidMetadataJson",
    "MbtilesFileNotFound",
    "MbtilesReloadError",
    "MissingMetadataJson",
]


class FileSystemAdapterError(Exception):
    """Error related to the file_system adapter."""


@dataclass
class IndicatorDataDirError(FileSystemAdapterError):
    indicator_dir: Path


class IndicatorDataDirNotFound(IndicatorDataDirError):
    pass


class DetectVersionError(IndicatorDataDirError):
    pass


class InvalidMetadataJson(IndicatorDataDirError):
    pass


class MissingMetadataJson(IndicatorDataDirError):
    pass


@dataclass
class MbtilesFileError(FileSystemAdapterError):
    mbtiles_file: Path


class MbtilesReloadError(FileSystemAdapterError):
    pass


class MbtilesFileNotFound(MbtilesFileError):
    pass
