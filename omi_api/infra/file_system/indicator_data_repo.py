import logging
import shutil
from dataclasses import dataclass, field
from pathlib import Path
from typing import Iterator, Optional

from omi_api.domain import IndicatorData
from omi_api.domain.entities.indicator_data import IndicatorDataId
from omi_api.infra.file_system.errors import (
    DetectVersionError,
    IndicatorDataDirNotFound,
    InvalidMetadataJson,
    MissingMetadataJson,
)
from omi_api.infra.file_system.model import IndicatorDataDir
from omi_api.infra.file_system.utils import get_file_size, iter_sub_directories

__all__ = ["FileSystemIndicatorDataRepo"]

log = logging.getLogger(__name__)


@dataclass
class FileSystemIndicatorDataRepo:
    indicator_data_base_dir: Path
    dry_run: bool = field(default=False)

    def get(self, indicator_data_id: IndicatorDataId) -> Optional[IndicatorData]:
        indicator_dir = self.indicator_data_base_dir / indicator_data_id
        indicator_data_dir = IndicatorDataDir.from_dir(indicator_dir, version="detect")
        indicator_data = indicator_data_dir.to_domain()
        return indicator_data

    def compute_size(self, indicator_data_id: IndicatorDataId) -> int:
        """Compute total size of files contained in given directorys (including sub-directories).

        Return 0 if the given directory is not found.
        """
        indicator_dir = self.indicator_data_base_dir / indicator_data_id
        if not indicator_dir.is_dir():
            return 0
        size_acc = 0
        for f in indicator_dir.rglob("*"):
            if (size := get_file_size(f)) is not None:
                size_acc += size
        return size_acc

    def iter_all(self) -> Iterator[IndicatorData]:
        for sub_dir in iter_sub_directories(self.indicator_data_base_dir):
            try:
                indicator_data_dir = IndicatorDataDir.from_dir(
                    sub_dir, version="detect"
                )
            except (DetectVersionError, InvalidMetadataJson, MissingMetadataJson):
                log.exception(
                    f"An exception occured while extracting metadata from {str(sub_dir)!r}"
                )
                continue

            indicator_data = indicator_data_dir.to_domain()
            if indicator_data is not None:
                yield indicator_data

    def delete(self, indicator_data_id: IndicatorDataId):
        indicator_dir = self.indicator_data_base_dir / indicator_data_id
        if not indicator_dir.is_dir():
            raise IndicatorDataDirNotFound(indicator_dir=indicator_dir)
        dry_run_info = " (dry run)" if self.dry_run else ""
        log.info(f"Delete indicator data dir {str(indicator_dir)}{dry_run_info}")
        if not self.dry_run:
            shutil.rmtree(indicator_dir)
