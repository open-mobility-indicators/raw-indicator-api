import logging
import subprocess
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from omi_api.domain.entities.indicator_data import IndicatorDataId
from omi_api.infra.file_system.errors import MbtilesFileNotFound, MbtilesReloadError
from omi_api.infra.file_system.utils import get_file_size

__all__ = ["FileSystemMbtilesRepo"]

log = logging.getLogger(__name__)

# mbtile file names are "{indicator-data-id}.mbtiles"
MbTileId = IndicatorDataId


@dataclass
class FileSystemMbtilesRepo:
    tileset_dir: Path
    mbtileserver_container_name: str
    dry_run: bool = False

    def compute_size(self, mbtile_id: MbTileId) -> Optional[int]:
        """Return mbtiles file in bytes if exists else 0."""
        mbtiles_file = self._mbtiles_file(mbtile_id)
        return get_file_size(mbtiles_file)

    def delete(self, mbtile_id: MbTileId):
        mbtiles_file = self._mbtiles_file(mbtile_id)
        if not mbtiles_file.exists():
            raise MbtilesFileNotFound(mbtiles_file=mbtiles_file)

        dry_run_info = " (dry run)" if self.dry_run else ""
        log.info(f"Delete mbtiles file {str(mbtiles_file)}{dry_run_info}")
        if not self.dry_run:
            mbtiles_file.unlink()

    def _mbtiles_file(self, mbtile_id: MbTileId) -> Path:
        return self.tileset_dir / f"{mbtile_id}.mbtiles"

    def reload_server(self) -> None:
        """Reload mbtiles server."""
        try:
            subprocess.check_call(
                ["docker", "kill", "-s", "SIGHUP", self.mbtileserver_container_name]
            )
        except subprocess.SubprocessError:
            log.exception("failed reloading mbtiles server")
            raise MbtilesReloadError("reload command failed")
