from datetime import datetime

from omi_api.domain import IndicatorData, ParameterProfile
from pydantic import AnyHttpUrl, BaseModel


class JsonIndicatorV1(BaseModel):
    created_at: datetime
    parameter_profile: str
    slug: str
    source_url: AnyHttpUrl


class MetadataJsonV1(BaseModel):
    """Parser class for metadata.json."""

    indicator: JsonIndicatorV1

    def to_domain(self, *, indicator_data_id: str) -> IndicatorData:
        """Transform instance to a IndicatorData instance of the domain model."""
        indicator = self.indicator
        parameter_profile = ParameterProfile(
            name=indicator.parameter_profile, values={}
        )
        from .common import build_source_from_source_url

        source = build_source_from_source_url(indicator.source_url)

        return IndicatorData(
            id=indicator_data_id,
            created_at=indicator.created_at,
            parameter_profile=parameter_profile,
            pipeline_id="",
            slug=indicator.slug,
            source=source,
        )
