from dataclasses import dataclass
from datetime import datetime
from typing import Any

from omi_api.domain import IndicatorData, IndicatorSource, ParameterProfile
from pydantic import AnyHttpUrl, BaseModel


@dataclass
class JsonParameterProfileV3:
    name: str
    values: dict[str, Any]

    def to_domain(self) -> ParameterProfile:
        return ParameterProfile(name=self.name, values={**self.values})


@dataclass
class JsonSourceV3:
    branch: str
    commit: str
    project_url: AnyHttpUrl

    def to_domain(self) -> IndicatorSource:
        return IndicatorSource(
            branch=self.branch, commit=self.commit, project_url=self.project_url
        )


class JsonIndicatorV3(BaseModel):
    created_at: datetime
    parameter_profile: JsonParameterProfileV3
    pipeline_id: str
    slug: str
    source: JsonSourceV3


class MetadataJsonV3(BaseModel):
    """Parser class for metadata.json."""

    indicator: JsonIndicatorV3

    def to_domain(self, *, indicator_data_id: str) -> IndicatorData:
        """Transform instance to a IndicatorData instance of the domain model."""
        indicator = self.indicator
        return IndicatorData(
            id=indicator_data_id,
            created_at=indicator.created_at,
            parameter_profile=indicator.parameter_profile.to_domain(),
            pipeline_id=indicator.pipeline_id,
            slug=indicator.slug,
            source=indicator.source.to_domain(),
        )
