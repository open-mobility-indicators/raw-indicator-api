from dataclasses import dataclass
from datetime import datetime
from typing import Any

from omi_api.domain import IndicatorData, ParameterProfile
from pydantic import AnyHttpUrl, BaseModel


@dataclass
class JsonParameterProfileV2:
    name: str
    values: dict[str, Any]

    def to_domain(self) -> ParameterProfile:
        return ParameterProfile(name=self.name, values={**self.values})


class JsonIndicatorV2(BaseModel):
    created_at: datetime
    parameter_profile: JsonParameterProfileV2
    slug: str
    source_url: AnyHttpUrl


class MetadataJsonV2(BaseModel):
    """Parser class for metadata.json."""

    indicator: JsonIndicatorV2

    def to_domain(self, *, indicator_data_id: str) -> IndicatorData:
        """Transform instance to a IndicatorData instance of the domain model."""
        indicator = self.indicator
        from .common import build_source_from_source_url

        source = build_source_from_source_url(indicator.source_url)

        return IndicatorData(
            id=indicator_data_id,
            created_at=indicator.created_at,
            parameter_profile=indicator.parameter_profile.to_domain(),
            pipeline_id="",
            slug=indicator.slug,
            source=source,
        )
