"""Model classes common to all versions."""

import json
from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import partial
from pathlib import Path
from typing import Optional

from omi_api.domain import IndicatorData, IndicatorSource
from pydantic import ValidationError
from pydantic.main import BaseModel

from ..errors import (
    DetectVersionError,
    IndicatorDataDirError,
    InvalidMetadataJson,
    MissingMetadataJson,
)
from .versions import ResolvableVersion, Version, get_class_for_version, resolve_version

__all__ = ["IndicatorDataDir"]


class PhysicalBaseModel(ABC, BaseModel):
    @abstractmethod
    def to_domain(self, *, indicator_data_id: str) -> IndicatorData:
        """Transform instance to a IndicatorData instance of the domain model."""


def detect_version_from_indicator_dir(indicator_dir: Path) -> Optional[Version]:
    metadata_json_path = get_metadata_json_path(indicator_dir)
    metadata_json = json.loads(metadata_json_path.read_text())
    return detect_version_from_metadata_json(metadata_json)


def detect_version_from_metadata_json(metadata_json: dict) -> Optional[Version]:
    version = metadata_json.get("version")
    if version is not None:
        return version

    indicator = metadata_json.get("indicator")
    if indicator is None:
        return None

    parameter_profile = indicator.get("parameter_profile")
    if isinstance(parameter_profile, str):
        return "1"

    source_url = indicator.get("source_url")
    if isinstance(source_url, str):
        return "2"

    pipeline_id = indicator.get("pipeline_id")
    if isinstance(pipeline_id, str):
        return "3"

    return None


def get_metadata_json_path(indicator_dir: Path) -> Path:
    return indicator_dir / "metadata.json"


def load_metadata_json(indicator_dir: Path, *, version: Version) -> PhysicalBaseModel:
    metadata_json_path = get_metadata_json_path(indicator_dir)

    if not metadata_json_path.is_file():
        raise MissingMetadataJson(indicator_dir=indicator_dir)

    metadata_json_class = get_class_for_version(
        class_name="MetadataJson", version=version
    )

    try:
        return metadata_json_class.parse_file(metadata_json_path)
    except ValidationError as exc:
        raise InvalidMetadataJson(indicator_dir=indicator_dir) from exc


@dataclass
class IndicatorDataDir:
    indicator_dir: Path
    metadata_json: PhysicalBaseModel

    def __post_init__(self):
        if not self.indicator_dir.is_dir():
            raise IndicatorDataDirError(indicator_dir=self.indicator_dir)

    @classmethod
    def from_dir(
        cls, indicator_dir: Path, *, version: ResolvableVersion = "latest"
    ) -> "IndicatorDataDir":
        """Loads indicator metadata from a directory path (normally) containing a metadata.json file."""

        if not (indicator_dir / "metadata.json").is_file():
            raise MissingMetadataJson(indicator_dir=indicator_dir)

        resolved_version = resolve_version(
            version,
            detect_version=partial(
                detect_version_from_indicator_dir, indicator_dir=indicator_dir
            ),
        )
        if resolved_version is None:
            raise DetectVersionError(indicator_dir=indicator_dir)

        metadata_json = load_metadata_json(indicator_dir, version=resolved_version)

        return IndicatorDataDir(
            indicator_dir=indicator_dir, metadata_json=metadata_json
        )

    @property
    def dir_name(self) -> str:
        return self.indicator_dir.name

    def to_domain(self) -> IndicatorData:
        return self.metadata_json.to_domain(indicator_data_id=self.dir_name)


def build_source_from_source_url(source_url: str) -> IndicatorSource:
    """Build IndicatorSource object, trying to extract project_url and commit hash from source_url."""
    commit, project_url = "", ""
    separator = "/-/tree/"
    if separator in source_url:
        (project_url, commit) = source_url.split(separator)
    return IndicatorSource(branch="", commit=commit, project_url=project_url)
