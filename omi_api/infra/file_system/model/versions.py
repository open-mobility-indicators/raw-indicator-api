from typing import Callable, Literal, Optional, Type, Union

from .v1 import MetadataJsonV1
from .v2 import MetadataJsonV2
from .v3 import MetadataJsonV3

Version = str
ResolvableVersion = Union[Literal["latest"], Literal["detect"], Version]

all_versions = ["1", "2", "3"]

class_by_version: dict[Version, dict[str, Type]] = {
    "1": {
        "MetadataJson": MetadataJsonV1,
    },
    "2": {
        "MetadataJson": MetadataJsonV2,
    },
    "3": {
        "MetadataJson": MetadataJsonV3,
    },
}


def get_class_for_version(*, class_name: str, version: Version) -> Type:
    return class_by_version[version][class_name]


def resolve_version(
    resolvable_version: ResolvableVersion,
    *,
    detect_version=Callable[[], Optional[Version]]
) -> Optional[Version]:
    if resolvable_version == "latest":
        return all_versions[-1]

    if resolvable_version == "detect":
        return detect_version()

    return resolvable_version
