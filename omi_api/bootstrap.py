from fastapi import FastAPI

from .infra.container import Container
from .infra.env.config import Settings
from .infra.web import app, router


def create_app() -> FastAPI:
    # TODO logging

    settings = Settings()

    container = Container()
    container.config.from_pydantic(settings)
    container.wire(modules=[app, router])

    return app.create_app()
