import abc
from enum import Enum
from typing import Iterator, Literal, Optional, Union

from omi_api.domain.entities.indicator import Indicator, IndicatorSlug
from pydantic import BaseModel

from .common import PaginationDef, SortOrderEnum
from .metadata_repository import CatalogMetadataRepository, IndicatorMetadataRepository

__all__ = ["IndicatorDataMode", "IndicatorSortDef", "IndicatorSortProperty"]


IndicatorDataMode = Union[None, Literal["ids", "full"]]


class IndicatorSortProperty(str, Enum):
    name = "name"
    slug = "slug"


class IndicatorSortDef(BaseModel):
    property_name: IndicatorSortProperty
    order: SortOrderEnum


IndicatorParameterProfiles = Union[Literal["all"], list[str]]


class IndicatorRepository(abc.ABC):
    def count(self, pre_selected: bool) -> int:
        pass

    def list(
        self,
        *,
        pre_selected: bool,
        sort_by: Optional[list[IndicatorSortDef]] = None,
        paginate_by: PaginationDef = None,
    ) -> Iterator[Indicator]:
        pass

    def get(self, indicator_slug: IndicatorSlug) -> Optional[Indicator]:
        pass

    def delete_by_slug(self, indicator_slug: IndicatorSlug) -> bool:
        pass

    def reload_from_slug_iter(
        self,
        slug_iter: Iterator[IndicatorSlug],
        *,
        indicator_metadata_repo: IndicatorMetadataRepository,
        catalog_metadata_repo: CatalogMetadataRepository,
    ):
        pass


class GitLabIndicatorProject(BaseModel):
    project_id: int
    ref: str
    parameter_profiles: set[str]


PipelineID = str


class IndicatorPipelineRepository(abc.ABC):
    def get_project_by_slug_and_branch(
        self, indicator_slug: IndicatorSlug, branch_name: str = None
    ) -> GitLabIndicatorProject:
        pass

    def run_pipeline(
        self,
        gitlab_indicator_project: GitLabIndicatorProject,
        parameter_profile: str,
    ) -> PipelineID:
        pass
