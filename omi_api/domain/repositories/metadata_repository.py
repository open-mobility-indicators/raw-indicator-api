import abc
from typing import Optional

from omi_api.domain.entities.catalog import CatalogIndicatorMetadata
from omi_api.domain.repositories.indicator_repository import IndicatorSlug


class CatalogMetadataRepository(abc.ABC):
    def is_preselected(self, indicator_slug: str, indicator_data_id: str) -> bool:
        pass

    def get_indicator_info(
        self, indicator_slug: IndicatorSlug
    ) -> Optional[CatalogIndicatorMetadata]:
        pass

    def reload(self):
        pass


class IndicatorMetadataRepository(abc.ABC):
    def get_indicator_metadata(self, indicator_slug: IndicatorSlug):
        pass

    def refresh(self):
        pass
