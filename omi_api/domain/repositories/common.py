from dataclasses import dataclass, field
from enum import Enum
from typing import Generic, Literal, Optional, TypeVar, Union

from pydantic import BaseModel

__all__ = ["PaginatedResults", "PaginationDef", "StatsMode"]


T = TypeVar("T")


class SortOrderEnum(str, Enum):
    asc = "asc"
    desc = "desc"


class PaginatedResults(BaseModel, Generic[T]):
    items: list[T]
    page: int
    size: int
    total: int


class PaginationDef(BaseModel):
    page: int
    size: int

    @property
    def limit(self):
        return self.size

    @property
    def offset(self):
        return (self.page - 1) * self.size


StatsMode = Union[None, Literal["true"]]

ReloadSubject = Union[
    None, Literal["catalog", "indicator", "indicator-data", "mbtileserver"]
]

# Command result status
CommandStatus = Union[Literal["ok"], Literal["error"]]


@dataclass
class Command:
    """Represent a run command by its name, result status and potential message."""

    name: str
    status: CommandStatus
    message: Optional[str] = None

    @staticmethod
    def from_success(command_name: str, *, message: str = None):
        return Command(name=command_name, status="ok", message=message)

    @staticmethod
    def from_error(command_name: str, message: str):
        return Command(name=command_name, status="error", message=message)

    @staticmethod
    def from_exception(command_name: str, ex: Exception):
        return Command.from_error(command_name, str(ex))


@dataclass
class CommandReport:
    """Report the result of several commands."""

    commands: list[Command] = field(default_factory=list)

    def add_cmd(self, cmd: Command):
        self.commands.append(cmd)
