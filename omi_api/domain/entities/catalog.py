from dataclasses import dataclass, field
from typing import Optional

from .indicator import IndicatorSlug
from .indicator_data import IndicatorDataId

__all__ = ["CatalogMetadata"]


@dataclass
class CatalogIndicatorDataMetadata:
    pre_selected: bool = field(default=False)


@dataclass
class CatalogIndicatorMetadata:
    pre_selected: Optional[bool] = field(default=False)
    work_in_progress: Optional[bool] = field(default=False)
    indicator_data: dict[IndicatorDataId, CatalogIndicatorDataMetadata] = field(
        default_factory=dict
    )


@dataclass
class CatalogMetadata:
    indicators: dict[IndicatorSlug, CatalogIndicatorMetadata]

    def get_indicator_info(
        self, indicator_slug: str
    ) -> Optional[CatalogIndicatorMetadata]:
        return self.indicators.get(indicator_slug)

    def is_preselected(self, indicator_slug: str, indicator_data_id: str) -> bool:
        if (indicator_info := self.get_indicator_info(indicator_slug)) is None:
            return False
        if (
            indicator_info.indicator_data is None
            or indicator_data_id not in indicator_info.indicator_data
        ):
            return False
        indicator_data_info = indicator_info.indicator_data[indicator_data_id]
        return (
            indicator_data_info.pre_selected
            if indicator_data_info.pre_selected
            else False
        )
