from dataclasses import dataclass, field
from typing import Optional

__all__ = ["Indicator", "IndicatorSlug"]


IndicatorSlug = str


@dataclass
class Indicator:
    description: Optional[str]
    name: str
    popup_template: Optional[str]
    slug: IndicatorSlug
    pre_selected: bool = field(default=False)
    work_in_progress: bool = field(default=False)
