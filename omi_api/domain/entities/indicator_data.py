from dataclasses import dataclass, field
from datetime import datetime
from typing import Any

__all__ = ["IndicatorDataId", "IndicatorData", "ParameterProfile", "IndicatorSource"]

IndicatorDataId = str


@dataclass
class ParameterProfile:
    name: str
    values: dict[str, Any]


@dataclass
class IndicatorSource:
    branch: str
    commit: str
    project_url: str  # TODO validate URL in string


@dataclass
class IndicatorData:
    id: IndicatorDataId
    created_at: datetime
    parameter_profile: ParameterProfile
    pipeline_id: str
    slug: str
    source: IndicatorSource
    pre_selected: bool = field(default=False)
