from dataclasses import dataclass
from typing import Optional, Sequence

from omi_api.domain.entities.indicator import Indicator, IndicatorSlug
from omi_api.domain.entities.indicator_data import IndicatorData
from omi_api.domain.entities.stat import Stat
from omi_api.domain.repositories.common import (
    Command,
    CommandReport,
    PaginationDef,
    ReloadSubject,
)
from omi_api.domain.repositories.indicator_data_repository import IndicatorDataFilterDef
from omi_api.domain.repositories.indicator_repository import (
    IndicatorParameterProfiles,
    IndicatorPipelineRepository,
    IndicatorRepository,
    IndicatorSortDef,
)
from omi_api.domain.repositories.metadata_repository import (
    CatalogMetadataRepository,
    IndicatorMetadataRepository,
)
from omi_api.infra.gitlab.errors import BranchNotFound, GitLabProjectNotFound

from .indicator_data_service import IndicatorDataService


@dataclass
class IndicatorService:
    indicator_data_service: IndicatorDataService
    catalog_metadata_repo: CatalogMetadataRepository
    indicator_pipeline_repo: IndicatorPipelineRepository
    indicator_metadata_repo: IndicatorMetadataRepository
    indicator_repo: IndicatorRepository

    def list_indicators(
        self,
        *,
        pre_selected: bool = False,
        sort_by: Optional[list[IndicatorSortDef]] = None,
        paginate_by: PaginationDef,
    ) -> tuple[int, Sequence[Indicator]]:
        return (
            self.indicator_repo.count(pre_selected=pre_selected),
            list(
                self.indicator_repo.list(
                    pre_selected=pre_selected, sort_by=sort_by, paginate_by=paginate_by
                )
            ),
        )

    def get_indicator_by_slug(
        self, indicator_slug: IndicatorSlug
    ) -> Optional[Indicator]:
        return self.indicator_repo.get(indicator_slug)

    def run_indicator_pipelines(
        self,
        indicator_slug: IndicatorSlug,
        *,
        parameter_profiles: IndicatorParameterProfiles,
        branch_name: str = None,
    ) -> CommandReport:

        # Get project
        try:
            indicator_project = (
                self.indicator_pipeline_repo.get_project_by_slug_and_branch(
                    indicator_slug, branch_name
                )
            )
        except GitLabProjectNotFound as ex:
            return CommandReport(commands=[Command.from_exception("get project", ex)])
        except BranchNotFound as ex:
            return CommandReport(commands=[Command.from_exception("get branch", ex)])

        # Check parameter profiles
        if parameter_profiles != "all":
            cmds: list[Command] = []
            cmd_name = "get profile"
            for profile in parameter_profiles:
                if profile not in indicator_project.parameter_profiles:
                    cmds.append(
                        Command.from_error(cmd_name, f"profile not found: {profile!r}")
                    )
            if cmds:
                return CommandReport(commands=cmds)

        # Compute profiles to use for pipelines
        pipeline_profiles = (
            indicator_project.parameter_profiles
            if parameter_profiles == "all"
            else parameter_profiles
        )

        # Run pipeline(s)
        cmds = []
        for profile_name in pipeline_profiles:
            job_name = f"create pipeline for {profile_name!r}"
            try:
                pipeline_id = self.indicator_pipeline_repo.run_pipeline(
                    indicator_project, profile_name
                )
                cmds.append(
                    Command.from_success(
                        job_name, message=f"pipeline id: {pipeline_id}"
                    )
                )
            except Exception as ex:
                cmds.append(Command.from_exception(job_name, ex))
        return CommandReport(commands=cmds)

    def delete_indicator_by_slug(self, indicator_slug: IndicatorSlug) -> CommandReport:
        command_report = CommandReport()
        deleted = self.indicator_repo.delete_by_slug(indicator_slug)
        name = f"delete indicator {indicator_slug!r}"
        if not deleted:
            command_report.add_cmd(
                Command(name=name, status="error", message="indicator not found")
            )
            return command_report
        command_report.add_cmd(Command.from_success(name))

        indicator_data_list = self._find_indicator_data_by_slug(indicator_slug)
        for indicator_data in indicator_data_list:
            delete_command_report = (
                self.indicator_data_service.delete_indicator_data_by_id(
                    indicator_data.id, reload=False
                )
            )
            for delete_cmd in delete_command_report.commands:
                command_report.add_cmd(delete_cmd)
        command_report.add_cmd(self.indicator_data_service.reload())
        command_report.add_cmd(self.indicator_data_service.reload_mbtileserver())
        command_report.add_cmd(self._reload_indicator())

        return command_report

    def _find_indicator_data_by_slug(
        self, indicator_slug: IndicatorSlug
    ) -> Sequence[IndicatorData]:
        filter_def = IndicatorDataFilterDef(slug=indicator_slug)
        _, indicator_data_list = self.indicator_data_service.find_indicator_data(
            filter_by=filter_def
        )
        return indicator_data_list

    def compute_stat(self, indicator_slug: IndicatorSlug) -> Stat:
        indicator_data_list = self._find_indicator_data_by_slug(indicator_slug)
        mbtiles, output_files = 0, 0
        for indicator_data in indicator_data_list:
            indicator_data_stat = self.indicator_data_service.compute_stat(
                indicator_data.id
            )
            if indicator_data_stat.mbtiles:
                mbtiles += indicator_data_stat.mbtiles
            if indicator_data_stat.output_files:
                output_files += indicator_data_stat.output_files
        return Stat(
            mbtiles=mbtiles, output_files=output_files, total=mbtiles + output_files
        )

    def reload(self, *, subject: ReloadSubject = None) -> CommandReport:

        command_report = CommandReport()

        if subject is None or subject == "catalog":
            command_report.add_cmd(self._reload_catalog())

        if subject is None or subject == "indicator-data":
            command_report.add_cmd(self.indicator_data_service.reload())

        if subject is None or subject in {"catalog", "indicator"}:
            command_report.add_cmd(self._reload_indicator())

        if subject is None or subject == "mbtileserver":
            command_report.add_cmd(self.indicator_data_service.reload_mbtileserver())

        return command_report

    def _reload_catalog(self) -> Command:
        self.catalog_metadata_repo.reload()
        return Command.from_success("reload catalog metadata")

    def _reload_indicator(self) -> Command:
        self.indicator_metadata_repo.refresh()
        self.indicator_repo.reload_from_slug_iter(
            self.indicator_data_service.iter_slug(),
            indicator_metadata_repo=self.indicator_metadata_repo,
            catalog_metadata_repo=self.catalog_metadata_repo,
        )
        return Command.from_success("reload indicator-metadata")
