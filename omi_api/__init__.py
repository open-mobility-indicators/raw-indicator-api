from .bootstrap import create_app

__all__ = ["create_app"]
