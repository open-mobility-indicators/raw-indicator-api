# CHANGELOG

## 0.3.1

- Fix parameter profiles retrieval
- (Hope to fix) reload problem on indicator removal

## 0.3.0

- Refactoring
- Add tests
- New endpoint `/indicator/{indicator_slug}/run_pipelines`
- New conf variable `OMI_API_GITLAB_PRIVATE_TOKEN`

## 0.2.4

- Sort indicator names case and diacritics insensitive
- Add tests

## 0.2.3

- Better report for reload and delete endpoints
- Implement indicator delete endpoint
- Reload endpoint now can reload `mbtileserver`
- Use slimmer container image

## 0.2.2

- Fix warnings
- implement mbtileserver reload
  - new settings parameter: `mbtileserver_container_name`

## 0.2.1

- Fix method call
- Clean code
- Update tests
- Remove useless CatalogMetadataWrapper class
- Reduce code using @dataclass
- Reformat .env.example (remove spaces around `=` signs)

## 0.2.0

- endpoints:
  - /indicator
  - /indicator/{indicator_slug}
  - /indicator/{indicator_slug} DELETE (not yet implemented)
  - /indicator-data
  - /indicator-data/{indicator_id}
  - /indicator-data/{indicator_id} DELETE
  - /\_reload
