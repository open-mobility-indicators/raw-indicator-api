FROM python:3.9-slim-bullseye

ENV PIP_NO_CACHE_DIR=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    POETRY_VERSION=1.1.8

EXPOSE 80

WORKDIR /app

# Docker install (to be able to reload mbtileserver)
RUN apt update && apt install -y apt-transport-https ca-certificates curl gnupg lsb-release
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt update
RUN apt install -y docker-ce-cli


RUN pip install poetry==$POETRY_VERSION

# Install only the dependencies, not the python package:
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-root --no-dev

# Copy in everything else and install:
COPY . .
RUN poetry install --no-dev

ENTRYPOINT ["poetry", "run", "uvicorn", "--factory", "omi_api:create_app", "--host", "0.0.0.0", "--port", "80"]